const { Op } = require("sequelize")

module.exports = async (req, Model, include = [], customQuery, order = null) => {
	try {
		const { limit, page, keyword } = req.query

		let params = {
			where: {}
		}
		
		if (customQuery) 
			params.where = customQuery(Op, req.query)

		if (keyword && keyword.length && !customQuery)
			params.where = { name: { [Op.iLike]: keyword } }

		const { count: total } = await Model.findAndCountAll(params)

		if (order) params.order = order

		params = {
			...params,
			include,
			offset: (page - 1) * limit,
			limit
		}

		const data = await Model.findAll(params)

		return { data, total }
	} catch(err) {
		return Promise.reject(err)
	}
}