const dayjs = require("dayjs")

exports.getDates = (startDate, stopDate) => {
	let dateArray = new Array()
	let currentDate = dayjs.utc(startDate).toDate()
	
	while (currentDate <= dayjs.utc(stopDate).toDate()) {
		dateArray.push( dayjs.utc(currentDate).format("YYYY-MM-DD") )
		currentDate = dayjs.utc(currentDate).add(1, "day").toDate()
	}

	return dateArray
}