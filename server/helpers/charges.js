const dayjs = require("dayjs"),
			coinbase = require("coinbase-commerce-node"),
			Charge = coinbase.resources.Charge,
			Client = coinbase.Client

Client.init(process.env.COINBASE)

exports.createCharge = async (order, car, member, extraAmount) => {
	try {
		const start = dayjs.utc(order.start),
					end = dayjs.utc(order.end)

		let params = {
			name: car.name,
			description: `${start.format("YYYY-MM-DD HH:mm")} - ${end.format("YYYY-MM-DD HH:mm")}`,
			local_price: {
				amount: `${(extraAmount || (parseFloat(order.amount) + parseFloat(order.amount) * .05)).toFixed(2)}`,
				currency: "USD"
			},
			brand_color: '#16305F',
			pricing_type: 'fixed_price',
			support_email: 'hello@dcr-motors.com',
      redirect_url: `${process.env.CLIENT_URI}/${member.locale}/charge/success`,
      cancel_url: `${process.env.CLIENT_URI}/${member.locale}/charge/canceled`
		}

		const charge = await Charge.create(params)

		return charge
	} catch(err) {
		console.error(err)
		return Promise.reject(err)
	}
}

exports.getCharge = id => Charge.retrieve(id)