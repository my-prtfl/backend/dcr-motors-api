const { Member, Messenger } = require("models")

module.exports = async (where, memberData, messengersData) => {
	let member = await Member.findOne({ where })

	if (!member) {
		member = await Member.create(memberData)

		for (let type in messengersData) {
			let messengers = messengersData[type]

			for (let name in messengers) {
				let value = messengers[name]

				if (value)
					await member.createMessenger({ name, extra: type === "extra" })
			}
		}
	} else {
		member = await member.update(memberData)

		const messengers = await member.getMessengers()
		
		for (let type in messengersData) {
			for (let name in messengersData[type]) {
				let value = messengersData[type][name]

				let exist = messengers.find(m => m.extra == (type === "extra") && m.name == name)

				if (value && !exist)
					member.createMessenger({ name, extra: type === "extra" })

				if (!value && exist)
					await Messenger.destroy({ where: { name, extra: type == "extra", memberID: member.id } })
			}
		}
	}

	return member
}