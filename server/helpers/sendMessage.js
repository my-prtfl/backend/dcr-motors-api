const mailer = require("mailer"),
			dayjs = require("dayjs")

module.exports = async (order, car, member, paymentLink) => {
	try {
		const start = dayjs.utc(order.start),
					end = dayjs.utc(order.end),
					range = `${start.format("YYYY-MM-DD HH:mm")} - ${end.format("YYYY-MM-DD HH:mm")}`

		const $t = {
			ru: {
				subject: `Оплата заказа #${order.id}`,
				text: `Оплата аренды ${car.name}`,
				html: `Оплатить аренду ${car.name} на время ${range} вы можете по ссылке (действует 1час): ${paymentLink}`
			},
			en: {
				subject: `Payment for order #${order.id}`,
				text: `Payment for rent ${car.name}`,
				html: `You can pay for ${car.name} rental at ${range} by following the link (valid for 1 hour): ${paymentLink}`
			}
		}
		
		await mailer.sendMail({
			from: `"Your Assistant" <${process.env.SMTP_U}>`,
			to: member.email,
			subject: $t[member.locale].subject,
			text: $t[member.locale].text,
			html: $t[member.locale].html
		})
		
	} catch(err) {
		console.error(err)
		return Promise.reject(err)
	}
}