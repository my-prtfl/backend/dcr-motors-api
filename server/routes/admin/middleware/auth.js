const { User } = require("models")

module.exports = async (req, res, next) => {
	try {
		const { admin_token } = req.cookies
		
		if (!admin_token) return res.sendStatus(401)

		const isAuthorized = await User.findOne({ where: { token: admin_token } })

		if (!isAuthorized)
			return res.sendStatus(401)

		res.locals.user = isAuthorized

		next()
	} catch(err) {
		return res.sendStatus(500)
	}
}