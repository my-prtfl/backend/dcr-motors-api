module.exports = (...levels) => {
	if (!levels.length) levels = [0, 1]

	return (req, res, next) => {
		if (!levels.includes(res.locals.user.role)) return res.sendStatus(403)

		next()
	}
}