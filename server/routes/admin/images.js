const { Router } = require("express"),
			{ Image } = require("models"),
			randToken = require("rand-token"),
			multer = require("storage/multer"),
			upload = require("storage/upload"),
			remove = require("storage/remove")

const router = Router()

router.route("/")
	// Получаем все загруженные изображения для страниц
	.get(async (req, res) => {
		try {
			const images = await Image.findAll({ where: { carID: null } })

			res.send({ images })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Загружаем новые изображения
	.post(multer("files", 10), async (req, res) => {
		try {
			const { files } = req

			// Собираем все промисы в один массив
			const promises = []
			for (let i in files) {
				const file = files[i]
				let name = `${randToken.generate(10)}`

				// Пушим загрузку в массив промисов
				promises.push(
					upload(file, name, "images"),
					Image.create({ name, position: 0, main: false })
				)
			}

			// Ожидаем завершения всех промисов
			await Promise.all(promises)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Удаляем выбранные файлы
	.put(async (req, res) => {
		try {
			const files = req.body

			const promises = []
			for (let file of files) {
				promises.push(
					remove(file.name, "images"),
					Image.destroy({ where: { id: file.id } })
				)
			}

			await Promise.all(promises)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router