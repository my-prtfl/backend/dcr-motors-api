const { Router } = require("express"),
			{ Op } = require("sequelize"),
			{ Brand } = require("models"),
			queryPaginated = require("helpers/queryPaginated")

const router = Router()

router.route("/")
	// Получаем список брендов с пагинацией и поиском по названию
	.get(async (req, res) => {
		try {
			const { data: brands, total } = await queryPaginated(req, Brand)

			res.send({ brands, total })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Создаём новый бренд
	.post(async (req, res) => {
		try {
			const { meta: metaData, brand: brandData } = req.body

			const brand = await Brand.create(brandData)

			// Создаём для бренда мету под каждый язык
			await Promise.all(
				Object.keys(metaData).map(lang => brand.createMeta({ ...metaData[lang], lang }))
			)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/all")
	// Получаем список всех брендов
	.get(async (req, res) => {
		try {
			const brands = await Brand.findAll()

			res.send(brands)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/check")
	// Проверяем существование бренда в бд
	.post(async (req, res) => {
		try {
			const { slug, exclude } = req.body

			const exist = await Brand.findOne({ where: { slug, id: { [Op.not]: exclude || 0 } } })

			res.send(!!exist)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:id([0-9]{1,})")
	// Достаём бренд по ID
	.get(async (req, res) => {
		try {
			const brand = await Brand.findByPk(req.params.id)

			if (!brand) return res.sendStatus(404)

			const meta = {};
			(await brand.getMetas()).forEach(m => meta[m.lang] = m)

			res.send({ brand, meta })
		} catch(err) {
			console.error(err)
		}
	})
	// Редактируем бренд
	.put(async ( req, res ) => {
		try {
			const { brand, meta } = req.body

			const db_brand = await Brand.findByPk(req.params.id)

			if (!db_brand) return res.sendStatus(404)

			const db_meta = await db_brand.getMetas()

			// Вносим и сохраняем изменения
			await Promise.all([
				db_brand.update(brand),
				...db_meta.map(m => m.update( meta[m.lang] ))
			])

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Удаляем бренд
	.delete(async ( req, res ) => {
		try {
			const brand = await Brand.findByPk(req.params.id)

			if (!brand) return res.sendStatus(404)

			const meta = await brand.getMetas()

			await Promise.all([
				brand.destroy(),
				...meta.map(m => m.destroy())
			])

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})


module.exports = router