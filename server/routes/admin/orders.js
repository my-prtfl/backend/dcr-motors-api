const { Router } = require("express"),
			{ Order, Member, Car, Image, Charge } = require("models"),
			{ Op } = require("sequelize"),
			queryPaginated = require("helpers/queryPaginated"),
			{ createCharge, getCharge } = require("helpers/charges"),
			dayjs = require("dayjs"),
			sendMessage = require("server/helpers/sendMessage")

const router = Router()

const datesFilter = (o, { date_from, date_to }) => {
	let data = {}

	if (date_from)
		data = {
			[o.or]: [
				{
					start: {
						[o.gte]: dayjs(date_from).startOf("day").toDate()
					}
				}
			]
		}

	if (date_to)
		data = {
			[o.or]: [
				...data[o.or],
				{
					end: {
						[o.lte]: dayjs(date_to).endOf("day").toDate()
					}
				}
			]
		}

	return data
}

router.route("/")
	// Достаём заказы из бд
	.get(async (req, res) => {
		try {
			const { keyword } = req.query

			const includes = [
				{
					model: Member
				},
				Car
			]

			if (keyword)
				includes[0].where = {
					[Op.or]: [
						{
							name: { [Op.iLike]: `%${keyword}%` }
						},
						{
							email: { [Op.iLike]: `%${keyword}%` }
						}
					]
				}

			const { data: orders, total } = await queryPaginated(req, Order, includes, datesFilter, [["createdAt", "DESC"]])

			res.send({ orders, total })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Создаем новый заказ
	.post(async (req, res) => {
		try {
			const orderData = req.body

			const carId = orderData.car.id
			delete orderData.car

			const memberId = orderData.client.id
			delete orderData.client

			const order = await Order.create({
				...orderData,
				byAdmin: true
			})

			await order.setCar(carId)
			await order.setMember(memberId)

			res.send(order)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:id([0-9]{1,})")
	// Достаём заказ из бд
	.get(async (req, res) => {
		try {
			const { id } = req.params

			const order = await Order.findOne({
				where: { id },
				order: [
					[{ model: Charge, as: "charges" }, "id", "DESC"]
				],
				include: [ 
					Member, 
					{
						model: Car,
						include: [{
							model: Image,
							where: {
								main: true
							}
						}]
					},
					{
						model: Charge,
						as: "charges"
					}
				]
			})

			if (!order) return res.sendStatus(404)

			const lastCharge = order.charges?.[order.charges?.length - 1]

			if (lastCharge) {
				switch (lastCharge.status) {
					case "created":
					case "pending":
					case "delayed":
						order.status = "paid"
						break
					case "resolved":
					case "confirmed":
						order.status = "confirmed"
						break
					case "unresolved":
					case "failed":
						order.status = "payError"
						break
				}

				await order.save()
			}

			res.send(order)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Подтверждаем заказ
	.put(async (req, res) => {
		try {
			const { id } = req.params,
						changes = req.body

			delete changes.Car
			delete changes.Member

			const order = await Order.findByPk(id)

			if (!order) return res.sendStatus(404)

			if (["canceled", "confirmed"].includes(changes.status)) {
				await order.update(changes)
				return res.sendStatus(200)
			}

			const member = await order.getMember()
			const car = await order.getCar({
				include: [ Image ]
			})
			
			const { 
				hosted_url: paymentLink, 
				id: cbid,
				created_at: createdAt,
				expires_at: expiresAt,
				timeline,
				payments,
				pricing,
				code
			} = await createCharge(order, car, member, changes.extraAmount)

			delete changes.extraAmount

			await order.createCharge({ cbid, code, paymentLink, expiresAt, createdAt, timeline, payments, pricing })
			
			changes.status = "paid"
			await order.update(changes)

			await sendMessage(order, car, member, paymentLink)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router