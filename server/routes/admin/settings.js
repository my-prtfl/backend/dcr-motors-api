const { Router } = require("express"),
			{ Setting, Meta } = require("models"),
			{ redis } = require("server/redis")

const router = Router()

router.route("/meta")
	// Вытаскиваем настройки метаданных главной страницы
	.get(async (req, res) => {
		try {
			const default_meta = await Meta.findAll({ where: { default: true } })
			let data = { ru: {}, en: {} }

			if (default_meta.length)
				default_meta.forEach(m => data[m.lang] = m)

			res.send(data)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Редактируем метаданные главной страницы
	.put(async (req, res) => {
		try {
			const { body } = req

			const meta = await Meta.findAll({ where: { default: true } })

			// Объявляем очередь промисов
			const pending = []
			if (meta.length)
				// Если дефолтная мета есть, редактируем её
				pending.push( ...meta.map(m => m.update( body[m.lang] )) )
			else
				// Иначе создаём ее
				pending.push( Meta.bulkCreate( 
					Object.keys(body).map(lang => ({ ...body[lang], lang, default: true }))
				))

			pending.push([
				redis.set("meta-ru", ""),
				redis.set("meta-en", "")
			])

			// Ждём заервешения создания/редактирования меты
			await Promise.all(pending)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:name")
	// Вытаскиваем параметр из бд по названию
	.get(async (req, res) => {
		try {
			const { name } = req.params

			let params = { where: { field_name: name } }

			const settings = await Setting.findAll(params)

			let data = settings[0]

			// Если параметров нашлось больше 1, значит второй под другой язык, распихиваем их по языкам
			if (settings.length > 1) {
				data = {}
				settings.forEach(s => data[s.lang] = JSON.parse(s.field_value))
			}

			res.send(data)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Редактируем параметр по имени
	.put(async (req, res) => {
		try {
			const { name } = req.params,
						{ body } = req

			let params = { where: { field_name: name } }
			const settings = await Setting.findAll(params)

			const changes = []

			if (!settings.length) {
				// Если параметры не найдены, создаём их в бд
				changes.push(
					Setting.bulkCreate(body)
				)
			} else {
				// Иначе редактируем найденные параметры
				changes.push(
					settings.map(s => s.update( body.find(({ lang }) => s.lang == lang) ))
				)
			}

			await Promise.all(changes)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router