const { Router } = require("express"),
			{ User } = require("models"),
			bcrypt = require("bcrypt"),
			randToken = require("rand-token")

const router = Router()

router.route("/")
	// Авторизация по логину/паролю
	.post(async (req, res) => {
		try {
			const { username, password } = req.body

			const user = await User.findOne({ where: { username } })

			if (!user)
				return res.status(404).send({ message: "user_not_found" })

			const validPassword = await bcrypt.compare(password, user.hash)

			if (!validPassword)
				return res.status(401).send({ message: "password_incorrect" })

			const token = randToken.generate(8)

			res.cookie("admin_token", token, { 
				maxAge: 7 * 24 * 60 * 60 * 1000,
				httpOnly: true,
				secure: process.env.NODE_ENV === 'production'? true: false
			})

			await user.update({ token })

			res.send(user)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Авторизация по токену
	.get(async (req, res) => {
		try {
			const token = req.cookies["admin_token"]
			
			if (!token) return res.sendStatus(401)

			const user = await User.findOne({ where: { token }, attributes: { exclude: [ "hash" ] } })

			if (!user) return res.clearCookie("admin_token").sendStatus(401)

			res.send(user)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Выход из сессии
	.delete(async (req, res) => {
		try {
			const { admin_token } = req.cookies
		
			if (!admin_token) return res.sendStatus(400)

			const user = await User.findOne({ where: { token: admin_token } })

			user.token = null
			await user.save()

			res.clearCookie("admin_token")
			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router