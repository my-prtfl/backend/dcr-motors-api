const { Router } = require("express"),
			{ Member, Order, Messenger, Car } = require("models"),
			queryPaginated = require("helpers/queryPaginated"),
			editOrCreateMember = require("helpers/editOrCreateMember")

const router = Router()

router.route("/")
	// Достаём пользователей из бд
	.get(async (req, res) => {
		try {
			const { data: clients, total } = await queryPaginated(req, Member, [ Order, Messenger ])

			res.send({ clients, total })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Создаем пользователя
	.post(async (req, res) => {
		try {
			const body = req.body

			const messengersData = body.messengers
			delete body.messengers

			await editOrCreateMember({ email: null }, body, messengersData)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:id([0-9]{1,})")
	// Достаём пользователя по id
	.get(async (req, res) => {
		try {
			const { id } = req.params

			const client = await Member.findByPk(id, {
				include: [ Messenger ]
			})

			if (!client) return res.sendStatus(404)

			const orders = await client.getOrders({ 
				include: [ Car ],
				order: [[ "id", "DESC" ]]
			})

			res.send({ client, orders })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Вносим изменения в объект пользователя
	.put(async (req, res) => {
		try {
			const { id } = req.params,
						changes = req.body

			const messengersData = changes.messengers
			delete changes.messengers

			const client = await Member.findByPk(id)

			if (!client) return res.sendStatus(404)

			await editOrCreateMember({ id }, changes, messengersData)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router