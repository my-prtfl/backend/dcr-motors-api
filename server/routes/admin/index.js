const { Router } = require("express")

const router = Router()

const auth = require("./middleware/auth")
const permissions = require("./middleware/permissions")

router.use("/auth", require("./auth"))
router.use("/brands", auth, permissions(), require("./brands"))
router.use("/cars", auth, permissions(), require("./cars"))
router.use("/orders", auth, permissions(), require("./orders"))
router.use("/settings", auth, permissions(), require("./settings"))
router.use("/clients", auth, permissions(), require("./clients"))
router.use("/users", auth, permissions(0), require("./users"))
router.use("/images", auth, permissions(), require("./images"))

console.log("Admin routes connected")

module.exports = router