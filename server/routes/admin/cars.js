const { Router } = require("express"),
			{ Brand, Car, Meta, Option, Image, Content } = require("models"),
			db = require("db"),
			queryPaginated = require("helpers/queryPaginated"),
			{ Op } = require("sequelize"),
			randToken = require("rand-token"),
			multer = require("storage/multer"),
			upload = require("storage/upload"),
			remove = require("storage/remove"),
			{ getDates } = require("server/helpers/datesRange")

const router = Router()

router.route("/")
	// Получаем список автомобилей с пагинацией и поиском по названию
	.get(async (req, res) => {
		try {
			const { data: cars, total } = await queryPaginated(req, Car, [ Image, Brand ], null, [["name", "ASC"]])

			res.send({ cars, total })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Создаём новый автомобиль
	.post(multer("files", 10), async (req, res) => {
		try {
			const data = JSON.parse(req.body.data),
						{ files } = req

			const car = await Car.create(data.car)

			// Собираем все промисы бд в один массив
			const carData = [
				// Устанавливаем автомобилю бренд
				car.setBrand(
					await Brand.findByPk(data.brand.id)
				),
				// Добавляем характеристики
				car.createOption(data.options),
				// Устанавливаем мету
				...Object.keys(data.meta).map(lang => car.createMeta({ ...data.meta[lang], lang })),
				// Устанавливаем контент на странице
				...Object.keys(data.content).map(lang => car.createContent({ ...data.content[lang], lang }))
			]

			// Собираем все промисы загрузок в один массив
			const uploads = []
			for (let i in files) {
				const file = files[i]
				let name = `${car.slug}_${randToken.generate(4)}`

				// Пушим загрузку в массив промисов загрузок
				uploads.push(
					upload(file, name)
				)

				// Пушим содание изображения в бд в массив промисов бд
				carData.push(
					car.createImage({ name, position: i, main: i == 0 })
				)
			}

			// Ожидаем завершения всех промисов
			await Promise.all([ ...carData, ...uploads ])

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/check")
	// Проверяем, существует ли машина с таким брендом и сылкой в бд
	.post(async (req, res) => {
		try {
			const { slug, brand: id, exclude } = req.body

			const brand = await Brand.findByPk(id)

			const cars = await brand.getCars({ where: { slug, id: { [Op.not]: exclude || 0 } } })

			res.send(!!cars.length)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/dates")
	// Выстаскиваем из бд все даты, на которые занят этот автомобиль
	.get(async (req, res) => {
		try {
			const { id, orderID } = req.query

			const query = `
				SELECT "Orders"."start", "Orders"."end"
				FROM "Cars"
					INNER JOIN "Orders"
						ON "Cars"."id" = "Orders"."carID"
				WHERE
					"Cars"."id" = '${id}'
					${orderID ? 'AND "Orders"."id" <> \'' + orderID + '\'' : ""}
					AND ("Orders"."status" = 'confirmed' OR "Orders"."byAdmin" = 'TRUE')
			`

			const [result] = await db.query(query)

			let data = []
			result.forEach(({ start, end }) => {
				let dates = getDates(start, end)
				data = [ ...data, ...dates ]
			})
			data = [...new Set(data)]

			console.log(data)

			res.send(data)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:id([0-9]{1,})")
	// Получаем автомобиль по ID
	.get(async (req, res) => {
		try {
			const car = await Car.findByPk(req.params.id)

			if (!car) return res.sendStatus(404)

			const [ brand, db_meta, options, images, db_content ] = await Promise.all([
				car.getBrand(), car.getMetas(), car.getOption(), car.getImages({ order: [["position", "ASC"]] }), car.getContents()
			])

			const meta = {}
			db_meta.forEach(m => meta[m.lang] = m)

			const content = {}
			if (db_content)
				db_content.forEach(c => content[c.lang] = c)

			res.send({ car, brand, meta, options, images, content })
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Редактируем автомобиль
	.put(multer("files", 10), async (req, res) => {
		try {
			const { car, brand, options, meta, images, content } = JSON.parse(req.body.data),
						{ files } = req

			const db_car = await Car.findByPk(req.params.id)

			if (!car) return res.sendStatus(404)

			// Массив очереди изменений
			const updates = []

			// Проверяем, какие картиночки удалили
			if (images && images.length) {
				for (let image of await db_car.getImages()) {
					let exist = images.find(({ id }) => image.id == id)

					if (!exist) {
						// Если такой картинки нет, то удаляем
						await Promise.all([ remove(image.name), image.destroy() ])
					} else {
						// Картинка есть, сохраняем её
						exist.main = exist.position == 0
						updates.push( image.update(exist) )
					}
				}
			} else {
				for (let image of await db_car.getImages()) {
					await Promise.all([ remove(image.name), image.destroy() ])
				}
			}

			// Загружаем картиночки, если они есть
			// Массив очереди загрузок
			if (files && files.length) {
				for (let file of files) {
					const position = parseInt(file.originalname.split(".")[0])
					let name = `${car.slug}_${randToken.generate(4)}`

					// Добавляем в очередь загрузок
					await upload(file, name)

					// Добавляем в очередь изменений
					updates.push(
						db_car.createImage({ name, position, main: position == 0 })
					)
				}
			}

			// // По очереди достаём все параметри из бд и проверяем, изменены ли они
			if (brand) {
				const db_brand = await db_car.getBrand()
				if (db_brand.id !== brand.id) {
					// Меняем бренд, если айдишники привязанного бренда и присланного не совпадают
					const new_brand = await Brand.findByPk(brand.id)
					updates.push( db_car.setBrand(new_brand) )
				}
			}

			if (options) {
				const db_options = await db_car.getOption()
				updates.push( db_options.update(options) )
			}

			if (meta) {
				const db_meta = await db_car.getMetas()
				updates.push(
					...db_meta.map(m => m.update( meta[m.lang] ))
				)
			}

			if (content) {
				const db_content = await db_car.getContents()
				updates.push(
					...db_content.map(c => c.update( content[c.lang] ))
				)
			}

			if (!car.stock)
				car.stock = 0

			// Добавляем в очередь изменения самой машины
			updates.push( db_car.update(car) )

			// Ждём окончания всеъ промисов
			await Promise.all( updates )

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Удаляем автомобиль
	.delete(async (req, res) => {
		try {
			const car = await Car.findByPk(req.params.id)

			if (!car) return res.sendStatus(404)

			const [ options, meta, images ] = await Promise.all([
				car.getOption(), car.getMetas(), car.getImages()
			])

			// Создаём очередь удалений
			const removes = [ car.destroy() ]

			if (options)
				removes.push( options.destroy() )

			if (meta.length)
				removes.push(
					...meta.map(m => m.destroy())
				)

			if (images.length)
					removes.push(
						...images.map(({ name }) => remove(name)),
						...images.map(i => i.destroy())
					)

			// Ждём, когда всё везде удалится
			await Promise.all( removes )

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router