const { Router } = require("express"),
			{ User } = require("models"),
			{ Op } = require("sequelize"),
			bcrypt = require("bcrypt")

const router = Router()

router.route("/")
	// Вытаскиваем пользователей админки
	.get(async (req, res) => {
		try {
			const users = await User.findAll({
				where: {
					username: {
						[Op.not]: "root"
					}
				}
			}) 

			res.send(users)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})
	// Создаём нового пользователя
	.post(async (req, res) => {
		try {
			const body = req.body
			
			const exist = !!(await User.findOne({ where: { username: body.username } }))
			if (exist) return res.status(400).send({ message: "user_exist" })

			const saltRounds = Math.floor( Math.random() * 8 )
			body.hash = await bcrypt.hash(body.password, saltRounds)

			delete body.passsword

			await User.create(body)

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/:id([0-9]{1,})")
	// Удаляем пользователя
	.delete(async (req, res) => {
		try {
			const { id } = req.params

			const user = await User.findByPk(id)
			if (!user) return res.sendStatus(404)

			await user.destroy()

			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router