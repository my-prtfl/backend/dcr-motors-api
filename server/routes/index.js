const { Router } = require("express"),
			cors = require("cors"),
			stream = require("stream"),
			storage = require("storage/storage"),
			dayjs = require("dayjs")

const router = Router()

router.route("/:bucket(images|cars)/:image(*.webp)")
	// Роут для выдачи картинок в img
	.get(async (req, res) => {
		try {
			const { image, bucket } = req.params

			const f = await storage.getObject(bucket, image).catch(() => null)

			if (!f) return res.sendStatus(404)

			const ps = new stream.PassThrough()

			stream.pipeline( f, ps, 
				err => {
					if (err) {
						console.error(err)
						return res.sendStatus(500)
					}
				}	
			)

			res.set({
				"Cache-control": "max-age=" + 60 * 60 * 24 * 365
				// "Expires": dayjs().add(1, "year").format("ddd, D MMM YYYY, HH:mm:ss") + " GMT"
			})

			ps.pipe(res)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

const adminCors = cors( require("server/common/cors")([ process.env.ADMIN_CORS ]) )
router.use("/admin", adminCors, require("./admin"))

const clientCors = cors( require("server/common/cors")([ ...process.env.CLIENT_CORS.split("|") ]) )
router.use("/api", clientCors, require("./client"))

module.exports = router