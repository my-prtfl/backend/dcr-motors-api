const { Router } = require("express"),
			{ Order, Member, Messenger, Car } = require("models"),
			dayjs = require("dayjs"),
			utc = require("dayjs/plugin/utc"),
			axios = require("axios"),
			editOrCreateMember = require("server/helpers/editOrCreateMember")

dayjs.extend(utc)

function convertDateToUTC(date) { return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()); }

const router = Router()

router.route("/")
	// Создаём заказ в бд
	.post(async (req, res) => {
		try {
			const { member: memberData, order: orderData } = req.body

			const car = await Car.findByPk(orderData.carID)
			delete orderData.carID

			const messengersData = memberData.messengers
			delete memberData.messengers

			const member = await editOrCreateMember({ email: memberData.email }, memberData, messengersData)

			const start = dayjs.utc(orderData.start),
						end = dayjs.utc(orderData.end),
						diff = end.diff(start, "minute"),
						d = Math.ceil(diff/60/24)

			orderData.amount = (car.stock ? car.price - (car.price / 100 * car.stock) : car.price) * d

			const order = await Order.create(orderData)
			await order.setCar(car)
			await member.addOrder(order)

			const admin_url = process.env.ADMIN_URI,
						token = process.env.TOKEN

			const text = `<b>New Request!</b>\n\n<b>Name:</b> <code>${member.name}</code>\n<b>E-mail:</b> <code>${member.email}</code>\n<b>Phone:</b> <code>${member.phone}</code>\n\n<b>Car:</b> <a href=\"//${admin_url}/cars/${car.id}\">${car.name}</a>\n\n<b>Date start:</b> <code>${dayjs.utc(order.start).format("YYYY-MM-DD HH:mm")}</code>\n<b>Place start:</b> <code>${order.receive}</code>\n\n<b>Date end:</b> <code>${dayjs.utc(order.end).format("YYYY-MM-DD HH:mm")}</code>\n<b>Place end:</b> <code>${order.return}</code>\n\n<b>Comment:</b> ${order.comment || "-"}`

			await axios.post(`https://api.telegram.org/bot${token}/sendMessage`, {
				chat_id: "-1001757576548",
				text,
				parse_mode: "HTML"
			})
			
			res.sendStatus(200)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router