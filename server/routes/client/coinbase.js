const { Router } = require("express"),
      { Charge } = require("models")

const router = Router()

// console.log(
//   JSON.stringify({
//     payment_id: '6ce4e911-c9b2-4a03-b564-bbaefec9e508',
//     network: 'bitcoin',
//     transaction_id: '1d772a4f21ab263bf86d7a0a8a162c9c0617dc67044a51f16e9acd237a19207a',
//     status: 'CONFIRMED',
//     detected_at: '2022-10-05T13:12:46Z',
//     value: {
//       local: { amount: '22.24', currency: 'USD' },
//       crypto: { amount: '0.00111081', currency: 'BTC' }
//     },
//     block: {
//       height: 757217,
//       hash: '00000000000000000006b7ed50f352654995f31902620dfab0a46d8fcc2cbe67',
//       confirmations: 153,
//       confirmations_required: 1
//     },
//     deposited: {
//       autoconversion_enabled: false,
//       autoconversion_status: 'PENDING',
//       status: 'COMPLETED',
//       destination: 'dubaicarental@gmail.com',
//       exchange_rate: null,
//       amount: { gross: [Object], net: [Object], coinbase_fee: [Object] }
//     },
//     coinbase_processing_fee: {
//       local: { amount: '0.22', currency: 'USD' },
//       crypto: { amount: '0.00001111', currency: 'BTC' }
//     },
//     net: {
//       local: { amount: '22.02', currency: 'USD' },
//       crypto: { amount: '0.00109970', currency: 'BTC' }
//     }
//   })
// )

router.route("/")
	.post(async (req, res) => {
    try {
      const data = req.body?.event?.data  
      // console.log(data)

      /*
      RESOLVED PAYMENT: {
        local: { amount: '100.0', currency: 'USD' },
        crypto: { amount: '10.00', currency: 'ETH' }
      }
      RESOLVED TIMELINE: [
        { time: '2018-01-01T00:00:00Z', status: 'NEW' },
        { status: 'EXPIRED', time: '2018-01-01T01:00:00Z' },
        {
          status: 'UNRESOLVED',
          time: '2018-01-02T00:00:00Z',
          context: 'DELAYED'
        },
        { status: 'RESOLVED', time: '2018-01-03T00:00:00Z' }
      ]
      */
      
      if (!data.id) return res.sendStatus(404)

      const charge = await Charge.findOne({ where: { cbid: data.id } })

      if (!charge) return res.sendStatus(404)

      const status = req.body.event.type.replace("charge:", "")
      const {
				timeline,
				payments
      } = data
      
      await charge.update({ timeline, payments, status })

      res.sendStatus(200)
    } catch(err) {
      console.error(err)
      res.sendStatus(500)
    }
	})

module.exports = router