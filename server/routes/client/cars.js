const { Router } = require("express"),
			{ Op } = require("sequelize"),
			db = require("db"),
			{ Car, Image, Brand, Option, Content, Order } = require("models"),
			{ getDates } = require("server/helpers/datesRange"),
			{ getValue, redis } = require("server/redis")

const router = Router()

router.route("/")
	// Вытаскиваем список авто с пагинацией
	.get(async (req, res) => {
		try {
			const { brand: slug } = req.query

			const includeBrand = {
				model: Brand
			}

			if (slug) {
				includeBrand.where = { slug }
			}

			params = {
				where: { active: true },
				include: [ 
					includeBrand, Option,
					{
						model: Image,
						where: {
							main: true
						}
					}
				],
				order: [ [ "id", "ASC" ] ]
			}

			const key = slug ? `cars-data-${slug}` : "cars-data"
			
			const data = await getValue(key, async () => {
				const { count: total } = await Car.findAndCountAll()

				const cars = await Car.findAll(params)

				return { total, cars }
			})

			res.send(data)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/dates")
	// Выстаскиваем из бд все даты, на которые занят этот автомобиль
	.get(async (req, res) => {
		try {
			const { car: carSlug, date } = req.query

			let start = `${date}T23:59:59.000Z`,
					end = `${date}T00:00:01.000Z`

			const query = `
				SELECT "Orders"."start", "Orders"."end"
				FROM "Cars"
					INNER JOIN "Orders"
						ON "Cars"."id" = "Orders"."carID"
				WHERE
					"Cars"."slug" = '${carSlug}'
					AND ("Orders"."status" = 'confirmed' OR "Orders"."byAdmin" = 'TRUE')
					AND ("Orders"."start"::date >= '${start}'::date
					OR "Orders"."end"::date >= '${end}'::date
					OR "Orders"."start"::date >= '${end}'::date
					OR "Orders"."end"::date >= '${start}'::date)
			`

			const [result] = await db.query(query)

			let data = []
			result.forEach(({ start, end }) => {
				let dates = getDates(start, end)
				data = [ ...data, ...dates ]
			})
			data = [...new Set(data)]

			res.send(data)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

router.route("/car")
	// Вытаскиваем из бд конкретный автомобиль
	.get(async (req, res) => {
		try {
			const { brand: brandSlug, car: carSlug } = req.query

			const brand = await Brand.findOne({ where: { slug: brandSlug } })

			if (!brand)
				return res.sendStatus(404)

			const car = await Car.findOne({
				where: { slug: carSlug, active: true },
				order: [
					[Image, "position", "asc"]
				],
				include: [ 
					Brand, Option, Content,
					{
						model: Image,
						where: {
							main: false
						}
					}
				]
			})

			if (!car)
				return res.sendStatus(404)

			res.send(car)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router