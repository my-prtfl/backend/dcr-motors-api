const { Router } = require("express"),
			db = require("db")

const router = Router()

router.route("/")
	// Получаем список брендов
	.get(async (req, res) => {
		try {
			const query = `
				SELECT
					b.name, b.slug, b.order
				FROM "Brands" b
					LEFT JOIN (
						SELECT
							"brandID" as bid,
							sum( 1 ) as total
						FROM "Cars" c
						GROUP BY bid
					) as cars_count
					ON cars_count.bid = b.id
				WHERE
					cars_count.total > 0
			`

			const [ brands ] = await db.query(query)

			res.send(brands)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router