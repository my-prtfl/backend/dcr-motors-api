const { Router } = require("express"),
			{ Brand, Car, Meta } = require("models"),
			{ getValue } = require("server/redis")

const router = Router()

router.route("/")
	// Вытаскиваем мету для любой страницы
	.get(async (req, res) => {
		try {
			const path = req.query.path.replace("/", "").split("/")
			let lang = path[0]
			let brand = path[1]
			let car = path[2]

			let meta

			if (brand && !car) {
				meta = await getValue(`meta-${lang}/${brand}`, async () => {
					const db_brand = await Brand.findOne({ where: { slug: brand }, include: [ Meta ] })

					if (!db_brand) return

					return db_brand.Meta?.find(({ lang: l }) => l == lang)
				})
			}

			if (car) {
				meta = await getValue(`meta-${lang}/${brand}/${car}`, async () => {
					const db_car = await Car.findOne({ where: { slug: car }, include: [ Meta ] })

					if (!db_car) return

					return car.Meta?.find(({ lang: l }) => l == lang)
				})
			}

			if (!car && !meta) {
				meta = await getValue(`meta-${lang}`, async () => {
					const db_meta = await Meta.findOne({ where: { lang, default: true } })

					if (!db_meta) return

					return db_meta
				})
			}
			
			res.send({
				title: meta?.title,
				description: meta?.description,
				keywords: meta?.keywords?.join(", ")
			})
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router