const { Router } = require("express"),
			fs = require("fs")

const router = Router()

fs.readdirSync(__dirname).forEach(file => {
	if (file !== "index.js") {
		var name = file.replace(".js", "")

		router.use(`/${name}`, require(`./${file}`))
	}
})
console.log("Client routes connected")

module.exports = router