const { Router } = require("express"),
			{ Member, Messenger } = require("models")

const router = Router()

router.route("/")
	// Выстаскиваем из бд пользователя
	.get(async (req, res) => {
		try {
			const { email } = req.query

			const member = await Member.findOne({ 
				where: { email },
				include: [ Messenger ] 
			})

			res.send(member)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router