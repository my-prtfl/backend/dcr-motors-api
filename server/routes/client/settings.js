const { Router } = require("express"),
			{ Setting } = require("models")

const router = Router()

router.route("/:name")
	// Получаем настройки из бд
	.get(async (req, res) => {
		try {
			const { name } = req.params

			let params = { where: { field_name: name } }
			const settings = await Setting.findAll(params)

			let data = settings[0]

			if (settings.length > 1) {
				data = {}
				settings.forEach(s => data[s.lang] = JSON.parse(s.field_value))
			}

			res.send(data)
		} catch(err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router