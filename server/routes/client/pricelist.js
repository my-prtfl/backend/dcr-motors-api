const { Router } = require("express"),
			puppeteer = require('puppeteer'),
			stream = require("stream")

const router = Router()

router.route("/")
	// Получаем pdf прайслиста
	.get(async (req, res) => {
		try {
			const params = { headless: true }

			if (process.env.NODE_ENV == "production") {
				params.executablePath = process.env.EXECUTABLE_PATH
				params.args = [ "--no-sandbox" ]
			}
			// /usr/bin/chromium-browser
			// /usr/bin/chromium

			const browser = await puppeteer.launch(params)
			const page = await browser.newPage()

			await page.goto('https://dcr-motors.com/ru/price', {waitUntil: 'networkidle0'})

			const pdf = await page.pdf({ 
				format: 'A4',
				displayHeaderFooter: true,
				printBackground: true
			})
		
			await browser.close();
			
			const s = stream.Readable.from(pdf)

			res.setHeader("Content-Type", "application/pdf")

			s.pipe(res)
		} catch (err) {
			console.error(err)
			res.sendStatus(500)
		}
	})

module.exports = router