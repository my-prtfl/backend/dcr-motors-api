const socket = require("socket.io"),
			cookieParser = require("socket.io-cookie-parser"),
			{ server } = require("./app"),
			checkAuthorization = require("./common/check_authorization")

const startPooling = () => {
	const io = new socket.Server(server, { 
		cors: require("./common/cors")
	})

	// Подключаем парсер куков для сокета
	io.use( cookieParser() )

	// Хук, срабатывающий при подключении сокета
	io.on("connection", async socket => {
		console.log(`${socket.id} connected`)

		// Проверяем, авторизован ли пользователь
		const isAuthorized = await checkAuthorization(socket.request.cookies?.auth)

		// Если не авторизован, отправляем на клиент 401 и закрываем соединение
		if (!isAuthorized) {
			socket.emit("401", "Unauthorized")
			socket.disconnect(true)
		}

		socket.on("close", socket.disconnect)
		socket.conn.on("close", () => {
			console.log(`${socket.id} disconnected`)
		})
	})

	return io
}

const io = startPooling()

module.exports = io