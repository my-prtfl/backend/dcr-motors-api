const { createClient } = require("redis")

let url = `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`

if (process.env.NODE_ENV == "production")
	url = `redis://:${process.env.REDIS_PWD}@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`

const client = createClient({ url })

;(async () => {
	try {
		await client.connect()
		await client.flushDb()
		console.log("Redis connected")
	} catch(err) {
		console.error("REDIS CONNECTION ERROR:", err)
		// console.error("REDIS CONNECTION ERROR:")
	}
})();

exports.redis = client

exports.getValue = async (key, cb) => {
	const cached = await client.get(key)

	if (cached) {
		console.log("Value for", key, "is cached")
		
		return JSON.parse(cached)
	}
	
	const value = await cb(key)

	console.log("Set value for", key)
	if (value)
		await client.set(key, JSON.stringify(value), {
			EX: 60 * 60 * 24 * 7
		})

	return value
}

exports.clearValue = async key => {

}