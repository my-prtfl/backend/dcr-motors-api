const { User } = require("models")

module.exports = token => User.findOne({ where: { token } })