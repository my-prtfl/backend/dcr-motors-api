module.exports = origins => ({
	origin: (o, cb) => {
		if (!o) o = ""
		return origins.includes(o) ? cb(null, true) : cb("Not allowed")
	},
	credentials: true,
	methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
	exposedHeaders: 'Set-Cookie',
	allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept',
	preflightContinue: false
})