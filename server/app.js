const express = require("express"),
			http = require("http"),
			{ json, urlencoded } = require("express"),
			helmet = require("helmet"),
			cookieParser = require("cookie-parser"),
			cors = require("cors")

const app = express()

app.use( cors( require("server/common/cors")([ ...process.env.CLIENT_CORS.split("|"), process.env.ADMIN_CORS ]) ) )
app.use( cookieParser() )
app.use( helmet({ contentSecurityPolicy: false }) )
app.use( json({ limit: "100mb" }) )
app.use( urlencoded({ extended: true }) )

const server = http.createServer(app)

exports.app = app
exports.server = server

const dayjs = require("dayjs"),
			utc = require("dayjs/plugin/utc")

dayjs.extend(utc)