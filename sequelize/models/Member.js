const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
  class Member extends Model {}
  
  Member.init({
    name: DataTypes.TEXT,
    email: DataTypes.TEXT,
    birthday: DataTypes.TEXT,
    phone: DataTypes.TEXT,
    extra_phone: DataTypes.TEXT,
    locale: {
      type: DataTypes.TEXT,
      defaultValue: "en"
    }
  }, {
		modelName: "Member",
		sequelize
	})

  return Member
}