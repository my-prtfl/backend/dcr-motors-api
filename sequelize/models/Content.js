const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
	class Content extends Model {}

	Content.init({
		data: DataTypes.TEXT,
		lang: DataTypes.TEXT
	}, {
		modelName: "Content",
		timestamps: false,
		sequelize
	})

	return Content
}