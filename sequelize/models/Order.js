const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
  class Order extends Model {}

  Order.init({
    receive: DataTypes.TEXT,
    return: DataTypes.TEXT,
    start: DataTypes.TEXT,
    end: DataTypes.TEXT,
    watched: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    comment: DataTypes.TEXT,
    paid: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    status: {
      type: DataTypes.TEXT,
      defaultValue: "created"
      /**
       * created
       * paid
       * confirmed
       * payError
       * inProgress
       * ended
       * canceled
       */
    },
    deposit: DataTypes.INTEGER,
    price: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    byAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
		modelName: "Order",
		updatedAt: false,
		sequelize
	})

  return Order
}