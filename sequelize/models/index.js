const db = require("../index")

/* Подключаем модельки к объекту sequelize */
exports.Charge = require("./Charge")(db)
exports.Brand = require("./Brand")(db)
exports.Car = require("./Car")(db)
exports.Content = require("./Content")(db)
exports.Image = require("./Image")(db)
exports.Member = require("./Member")(db)
exports.Messenger = require("./Messenger")(db)
exports.Meta = require("./Meta")(db)
exports.Option = require("./Option")(db)
exports.Order = require("./Order")(db)
exports.Partner = require("./Partner")(db)
exports.Setting = require("./Setting")(db)
exports.Site = require("./Site")(db)
exports.User = require("./User")(db)
exports.Visit = require("./Visit")(db)

/** Устанавливаем ассоциации в модельках */
exports.Car.hasMany(exports.Image, { foreignKey: 'carID' })
exports.Car.belongsTo(exports.Brand, { foreignKey: 'brandID' })
exports.Car.belongsTo(exports.Option, { foreignKey: 'OptionID' })
exports.Car.hasMany(exports.Meta, { foreignKey: 'carID' })
exports.Car.hasMany(exports.Order, { foreignKey: 'carID' })
exports.Car.hasMany(exports.Content, { foreignKey: 'carID' })

exports.Order.belongsTo(exports.Car, { foreignKey: 'carID' })
exports.Order.belongsTo(exports.Member, { foreignKey: 'memberID' })
exports.Order.hasOne(exports.Visit, { foreignKey: 'orderID' })
exports.Order.hasMany(exports.Charge, { as: "charges", foreignKey: "orderID" })
exports.Charge.hasOne(exports.Order, { as: "order", foreignKey: "orderID" })

exports.Member.hasMany(exports.Order, { foreignKey: 'memberID' })
exports.Member.belongsTo(exports.Partner, { foreignKey: 'partnerID' })
exports.Member.hasMany(exports.Messenger, { foreignKey: 'memberID' })
exports.Messenger.belongsTo(exports.Member, { foreignKey: 'memberID' })

exports.Image.belongsTo(exports.Car, { foreignKey: 'carID' })

exports.Brand.hasMany(exports.Meta, { foreignKey: 'brandID' })
exports.Brand.hasMany(exports.Car, { foreignKey: 'brandID' })

exports.Site.belongsTo(exports.Partner, { foreignKey: 'partnerID' })
exports.Visit.belongsTo(exports.Site, { foreignKey: 'siteID' })
exports.Site.hasMany(exports.Visit, { foreignKey: 'siteID' })
exports.Visit.belongsTo(exports.Partner, { foreignKey: 'partnerID' })
exports.Visit.belongsTo(exports.Order, { foreignKey: 'orderID' })
exports.Partner.hasMany(exports.Visit, { foreignKey: 'partnerID' })
exports.Partner.hasMany(exports.Site, { foreignKey: 'partnerID' })
exports.Partner.hasMany(exports.Member, { foreignKey: 'partnerID' })

exports.Partner.belongsTo(exports.Setting, { foreignKey: 'payoutID' })
exports.Setting.hasMany(exports.Partner, { foreignKey: 'payoutID' })

;(async () => {
	try {
		/* Синхронизируем модельки с базой данных */
		await db.sync({ alter: true })

		console.log("Models\x1b[32m",  Object.keys(db.models).map(item => item).join(', '), "\x1b[0mare in sync")
	} catch(err) {
		console.error("Ошибка соединения с бд:", err)
	}
})();