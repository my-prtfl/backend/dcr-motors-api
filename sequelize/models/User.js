const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
	class User extends Model {}

	User.init({
		username: DataTypes.TEXT,
		hash: DataTypes.TEXT,
		token: DataTypes.TEXT,
		role: {
			type: DataTypes.INTEGER,
			defaultValue: 1,
			allowNull: false
			/**
			 * 0 - Root
			 * 1 - Менеджер
			 */
		}
	}, {
		modelName: "User",
		timestamps: false,
		sequelize
	})

	return User
}