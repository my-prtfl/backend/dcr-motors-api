const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
	class Image extends Model {}

	Image.init({
		name: DataTypes.TEXT,
		position: DataTypes.INTEGER,
		main: {
			type: DataTypes.BOOLEAN,
			defaultValue: false
		}
	}, {
		modelName: "Image",
		timestamps: false,
		sequelize
	})

	return Image
}