const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
  class Setting extends Model {}

  Setting.init({
    field_name: DataTypes.TEXT,
    field_value: DataTypes.TEXT,
    lang: DataTypes.TEXT
  }, {
		modelName: "Setting",
		timestamps: false,
		sequelize
	})
  
  return Setting
}