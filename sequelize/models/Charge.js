const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
  class Charge extends Model {}

  Charge.init({
    cbid: DataTypes.TEXT,
    code: DataTypes.TEXT,
    paymentLink: DataTypes.TEXT,
    status: {
      type: DataTypes.TEXT,
      defaultValue: "created"
      /**
       * created
       * confirmed
       * delayed
       * failed
       * pending
       * unresolved
       * resolved
       */
    },
    timeline: DataTypes.JSONB,
    payments: DataTypes.JSONB,
    pricing: DataTypes.JSONB,
    createdAt: DataTypes.DATE,
    expiresAt: DataTypes.DATE,
    redirectUrl: DataTypes.TEXT,
    cancelUrl: DataTypes.TEXT
  }, {
		modelName: "Charge",
		timestamps: false,
		sequelize
	})

  return Charge
}