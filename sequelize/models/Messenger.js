const { DataTypes, Model } = require("sequelize")

module.exports = sequelize => {
	class Messenger extends Model {}

	Messenger.init({
		name: DataTypes.TEXT,
		extra: {
			type: DataTypes.BOOLEAN,
			defaultValue: false
		}
	}, {
		modelName: "Messenger",
		sequelize,
		timestamps: false
	})

	return Messenger
}