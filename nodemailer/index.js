const nodemailer = require("nodemailer")

/**
 * {
  user: 'cpvjvrvox2komno4@ethereal.email',
  pass: 'e46w5yZNZA3J5gduup',
  smtp: { host: 'smtp.ethereal.email', port: 587, secure: false },
  imap: { host: 'imap.ethereal.email', port: 993, secure: true },
  pop3: { host: 'pop3.ethereal.email', port: 995, secure: true },
  web: 'https://ethereal.email'
}
 */

const client = nodemailer.createTransport({
  host: process.env.SMTP_H,
  port: process.env.SMTP_PORT,
  secure: parseInt(process.env.SMTP_PORT) == 465,
  auth: {
    user: process.env.SMTP_U,
    type: "OAuth2",
    serviceClient: process.env.SMTP_CLIENT,
    privateKey: process.env.SMTP_KEY.replace(/\\n/g, '\n')
  }
})

;(async () => {
  try {
    await client.verify()
    console.log("Successfully connected to Gmail")
  } catch(err) {
    console.error(err)
  }
})();

module.exports = client