require('module-alias/register')
require("dotenv").config()

// Очищаем коноль при (пере)запуске
console.clear()

require("./server/index")