# DCR-Motors api

> DCR-motors is a Dubai car rental website

This project is an api for [DCR-Motors](https://dcr-motors.com)

## Stack
* Express
* PostgreSQL
* Sequelize
* [Minio](https://min.io/) (S3 storage)

## Functional:
* Authorization with cookies
* Manipulation with database using Sequelize
* Resizing images using [Sharp](https://www.npmjs.com/package/sharp) and uploading on Minio
* Email notifiy using [nodemailer](https://nodemailer.com/about/)
