const storage = require("./storage")

module.exports = (name, bucket = "cars") => {
	const vars = [ "", "_480", "_720", "_1280", "_1920" ]

	return new Promise(( resolve, reject ) => {
		storage.removeObjects(bucket, vars.map(v => `${name}${v}.webp`), err => {
			if (err) {
				console.log(err)
				return reject(err)
			}
			console.log(`Удалили картинку "${name}" из хранилища`)
			resolve(true)
		})
	})
}