const Minio = require("minio")

const params = {
	endPoint: process.env.MINIO_URL,
	// useSSL: process.env.NODE_ENV == "production",
	useSSL: false,
	accessKey: process.env.MINIO_LOGIN,
	secretKey: process.env.MINIO_PWD
}

if (process.env.NODE_ENV !== "production")
	params.port = parseInt(process.env.MINIO_PORT)

const client = new Minio.Client(params)

client.listBuckets()
	.then(() => console.log("Successfully connected to file storage"))
	.catch(err => {
		console.error(err)
		throw new Error("Minio connection error")
	})

module.exports = client